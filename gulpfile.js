"use strict";

var lr = require('tiny-lr'), // Минивебсервер для livereload
    gulp = require('gulp'), // Сообственно Gulp JS
    livereload = require('gulp-livereload'), // Livereload для Gulp
    sass = require('gulp-sass'),
    csso = require('gulp-csso'), // Минификация CSS
    imagemin = require('gulp-imagemin'), // Минификация изображений
    pngquant = require('imagemin-pngquant'),
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    minifyHTML = require('gulp-minify-html'),
    connect = require('connect'), // Webserver
    server = lr();

var paths = {
    img:'./assets/img/*',
    sass: './assets/sass/**/*.scss',
    js: './assets/js/**/*.js',
    html: './assets/template/*.html'
}

gulp.task('minify-html', function() {
  return gulp.src(paths.html)
    .pipe(minifyHTML({ empty: true }))
    .pipe(gulp.dest('./public'))
    .pipe(livereload());
});
    
gulp.task('images', function () {
	return gulp.src(paths.img)
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
		.pipe(gulp.dest('./public/images'))
    .pipe(livereload());
});
    
gulp.task('sass', function () {
  return gulp.src(paths.sass)
    .pipe(sass().on('error', sass.logError))
    .pipe(csso())
    .pipe(concat('all.min.css'))
    .pipe(gulp.dest('./public/css'))
    .pipe(livereload());
});

gulp.task('scripts', function() {
  return gulp.src(paths.js)
    .pipe(uglify())
    .pipe(concat('all.min.js'))
    .pipe(gulp.dest('./public/js'))
    .pipe(livereload());
});
 
gulp.task('watch', function () {
    livereload.listen();
    gulp.watch(paths.html, ['minify-html']);
    gulp.watch(paths.img, ['images']);
    gulp.watch(paths.sass, ['sass']);
    gulp.watch(paths.js, ['scripts']);
});

gulp.task('http-server', function() {
    connect()
        .use(require('connect-livereload')())
        .use(connect.static('./public'))
        .listen(process.env.PORT);

    console.log('Server listening on http://'+process.env.IP+':'+process.env.PORT);
});

gulp.task('default', ['watch','minify-html', 'images', 'sass', 'scripts', 'http-server']);