var APP = APP || {};

(function(){
    "use strict";
    var options = {version: "0.0.1a"};
    
    var FluteApp = Backbone.Marionette.Application.extend({
		setRootLayout: function () {
			this.root = new APP.MainLayout();
		}
	});
    
    APP.AudioContext = new (window.AudioContext || window.webkitAudioContext || window.mozAudioContext || window.oAudioContext || window.msAudioContext);
    
    APP.FluteApp = new FluteApp();
    
    APP.FluteApp.on('before:start', function () {
		console.log(options.version);
		APP.FluteApp.setRootLayout();
	});
	
	Marionette.ItemView = Marionette.ItemView.extend({ 
        modelEvents: {
            'change': 'fieldsChanged'
        },
        fieldsChanged: function() {
            this.render();
        }
	});

})();